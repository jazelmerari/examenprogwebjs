document.addEventListener("DOMContentLoaded", function () {
    var cantidadInput = document.getElementById("cantidad");
    var celsiusRadio = document.getElementById("celsiusRadio");
    var fahrenheitRadio = document.getElementById("fahrenheitRadio");
    var resultadoInput = document.getElementById("resultado");
    var calcularBtn = document.getElementById("calcularBtn");
    var limpiarBtn = document.getElementById("limpiarBtn");

    calcularBtn.addEventListener("click", function () {
        var cantidad = parseFloat(cantidadInput.value);

        if (!isNaN(cantidad)) {
            var unidad = celsiusRadio.checked ? "Celsius" : "Fahrenheit";
            var resultado = unidad === "Celsius" ? (cantidad - 32) * 5/9 : (cantidad * 9/5) + 32;

            resultadoInput.value = resultado.toFixed(2) + " " + unidad;
        }
    });

    limpiarBtn.addEventListener("click", function () {
        cantidadInput.value = "";
        resultadoInput.value = "";
    });
});