document.getElementById('calcular').addEventListener('click', function() {
    const numeroBoleto = document.getElementById('numeroBoleto').value;
    const nombreCliente = document.getElementById('nombreCliente').value;
    const destino = document.getElementById('destino').value;
    const tipoViaje = parseInt(document.getElementById('tipoViaje').value);
    const precio = parseFloat(document.getElementById('precio').value);
    
    let costo = precio;
    if (tipoViaje === 2) {
        costo *= 1.8; 
    }

    const subtotal = costo;
    const impuesto = costo * 0.16;
    const total = subtotal + impuesto;

    document.getElementById('subtotal').textContent = subtotal.toFixed(2);
    document.getElementById('impuesto').textContent = impuesto.toFixed(2);
    document.getElementById('total').textContent = total.toFixed(2);
});

document.getElementById('limpiar').addEventListener('click', function() {
    document.getElementById('numeroBoleto').value = '';
    document.getElementById('nombreCliente').value = '';
    document.getElementById('destino').value = '';
    document.getElementById('tipoViaje').value = '1';
    document.getElementById('precio').value = '';
    document.getElementById('subtotal').textContent = '0';
    document.getElementById('impuesto').textContent = '0';
    document.getElementById('total').textContent = '0';
});

document.getElementById('formulario').addEventListener('submit', function(event) {
    event.preventDefault();
    alert('Formulario enviado. Los datos se han registrado.');
});
