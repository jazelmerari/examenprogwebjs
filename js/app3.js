document.getElementById('generar').addEventListener('click', function() {
    generarEdadesAleatorias();
});

function generarEdadesAleatorias() {
    const edades = [];
    for (let i = 0; i < 100; i++) {
        edades.push(Math.floor(Math.random() * 91));
    }
    document.getElementById('edadesAleatorias').textContent = "Edades Aleatorias: " + edades.join(', ');
}

function calcularPromedio(edades) {
    let suma = 0;
    for (let i = 0; i < edades.length; i++) {
        suma += edades[i];
    }
    return suma / edades.length;
}

function calcularEdades() {
    const edadesTexto = document.getElementById('edadesAleatorias').textContent.replace("Edades Aleatorias: ", "");
    const edades = edadesTexto.split(', ').map(Number);
    const bebes = edades.filter(edad => edad >= 1 && edad <= 3).length;
    const niños = edades.filter(edad => edad >= 4 && edad <= 12).length;
    const adolescentes = edades.filter(edad => edad >= 13 && edad <= 17).length;
    const adultos = edades.filter(edad => edad >= 18 && edad <= 60).length;
    const ancianos = edades.filter(edad => edad >= 61).length;
    const edadPromedio = calcularPromedio(edades);

    document.getElementById('resultados').textContent = `Bebés: ${bebes}, Niños: ${niños}, Adolescentes: ${adolescentes}, Adultos: ${adultos}, Ancianos: ${ancianos}`;
    document.getElementById('resultados').textContent += `, Edad Promedio: ${edadPromedio.toFixed(2)} años`;
}

function limpiarResultados() {
    document.getElementById('edadesAleatorias').textContent = "";
    document.getElementById('resultados').textContent = "";
}

document.getElementById('calcular').addEventListener('click', function() {
    calcularEdades();
});

document.getElementById('limpiar').addEventListener('click', function() {
    limpiarResultados();
});
